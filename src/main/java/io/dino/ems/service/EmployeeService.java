package io.dino.ems.service;

import io.dino.ems.repository.EmployeeRepository;
import io.dino.ems.repository.entity.Employee;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

// TODO This should be interface implemented in a service class
@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Optional<Employee> get(Long id) {
        return employeeRepository.findById(id);
    }
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    public void deleteById(Long id) {
        employeeRepository.deleteById(id);
    }
}
