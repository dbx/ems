package io.dino.ems.controller;

import io.dino.ems.controller.assembler.EmployeeModelAssembler;
import io.dino.ems.exception.EmployeeNotFoundException;
import io.dino.ems.repository.entity.Employee;
import io.dino.ems.service.EmployeeService;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class EmployeeController {

    private final EmployeeService employeeService;
    private final EmployeeModelAssembler employeeModelAssembler;

    public EmployeeController(EmployeeService employeeService, EmployeeModelAssembler employeeModelAssembler) {
        this.employeeService = employeeService;
        this.employeeModelAssembler = employeeModelAssembler;
    }

    @GetMapping("/employees/{id}")
    public EntityModel<Employee> getOne(@PathVariable Long id) {
        Employee employee = employeeService.get(id).orElseThrow(() -> new EmployeeNotFoundException(id));
        return employeeModelAssembler.toModel(employee);
    }

    @GetMapping("/employees")
    public CollectionModel<EntityModel<Employee>> getAll() {
        List<EntityModel<Employee>> employees = employeeService.getAll()
                .stream()
                .map(employeeModelAssembler::toModel)
                .collect(Collectors.toList());

        return CollectionModel.of(employees
                , linkTo(methodOn(EmployeeController.class).getAll()).withSelfRel());
    }

    @PostMapping("/employees")
    public ResponseEntity<?> save(@RequestBody Employee employee) {
        EntityModel<Employee> entityModel = employeeModelAssembler.toModel(employeeService.save(employee));

        // Spring MVC will create HTTP 201 Created status message.  This type of response also aften has
        // a Location response header and we get the URI from models self-related link
        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
    }

    @PutMapping("/employees/{id}")
    ResponseEntity<?> update(@RequestBody Employee newEployee, @PathVariable Long id) {
        Employee updatedEmployee = employeeService.get(id)
                .map(employee -> {
                    employee.setName(newEployee.getName());
                    employee.setRole(newEployee.getRole());
                    return employeeService.save(employee);
                })
                .orElseGet(() -> {
                    newEployee.setId(id);
                    return  employeeService.save(newEployee);
                });

        EntityModel<Employee> entityModel = employeeModelAssembler.toModel(updatedEmployee);

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(entityModel);
    }

    @DeleteMapping("/employees/{id}")
    ResponseEntity<?> deleteEmployee(@PathVariable Long id) {
        employeeService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
