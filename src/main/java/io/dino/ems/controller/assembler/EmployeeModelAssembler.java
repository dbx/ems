package io.dino.ems.controller.assembler;

import io.dino.ems.controller.EmployeeController;
import io.dino.ems.repository.entity.Employee;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Converts Employee entity object to EntityModel<Employee> objects by use of Spring HATEOAS's
 * RepresentationModelAssembler.
 * Following HATEOS, it provides self and aggregate references (links) by reducing repetition by
 * implementing same in each endpoint repeatedly.
 */
@Component
public class EmployeeModelAssembler implements RepresentationModelAssembler<Employee, EntityModel<Employee>> {

    /**
     * Convert a non-model object Employee into a model-based object EntityModel<Employee>.
     * @param employee
     * @return
     */
    @Override
    public EntityModel<Employee> toModel(Employee employee) {
        return EntityModel.of(employee,
                linkTo(methodOn(EmployeeController.class).getOne(employee.getId())).withSelfRel(), //self
                linkTo(methodOn(EmployeeController.class).getAll()).withRel("employees")); //aggregate
    }
}
