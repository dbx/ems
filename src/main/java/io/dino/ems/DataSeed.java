package io.dino.ems;

import io.dino.ems.repository.EmployeeRepository;
import io.dino.ems.repository.entity.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DataSeed implements CommandLineRunner {

    private EmployeeRepository employeeRepository;

    public DataSeed(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        employeeRepository.save(new Employee("Dino", "Buljubasic", "Software Developer"));
        employeeRepository.save(new Employee("Amira",  "Imamovic-Buljubasic", "Molecular Biologist"));
    }
}
